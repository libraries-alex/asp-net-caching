using Microsoft.AspNetCore.Mvc;

namespace Alex.AspNetCore.Caching
{
    public interface ICacheIoFactory<in TInput, in TOutput>
    {
        ICacheInputItem GenerateInputItem(TInput input);
        ICacheOutputItem GenerateOutputItem(TOutput output);
    }
    
    public interface ICacheInputItem
    {
    }
    
    public interface ICacheOutputItem
    {
        IActionResult ToActionResult();
    }
}