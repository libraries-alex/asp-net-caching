﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace Alex.AspNetCore.Caching
{
    public class CachedResponseAttribute : TypeFilterAttribute , IFilterFactory
    {
        private readonly AbstractResponseCacheProvider _cacheProvider;

        public CachedResponseAttribute(Type cacheProvider) : base(typeof(CacheResponseActionFilter))
        {
            _cacheProvider = (AbstractResponseCacheProvider) Activator.CreateInstance(cacheProvider);
        }

        // Implement IFilterFactory
        public new IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            return new CacheResponseActionFilter(_cacheProvider);
        }

        private class CacheResponseActionFilter : IActionFilter
        {
            private readonly AbstractResponseCacheProvider _cacheProvider;
            public CacheResponseActionFilter(AbstractResponseCacheProvider cacheProvider)
            {
                _cacheProvider = cacheProvider;
            }
            public void OnActionExecuting(ActionExecutingContext context)
            {

                if (!context.HttpContext.Response.HasStarted)
                {
                    string cacheKey = _cacheProvider.GetCacheKey(context.HttpContext);
                    ICacheOutputItem cachedItem = _cacheProvider.GetCachedItem(cacheKey);
                    if (cachedItem != null && IsCachedEntryValid(cachedItem))
                    {
                        context.Result = cachedItem.ToActionResult();
                    }
                    else
                    {
                        _cacheProvider.MarkForCaching(context.HttpContext);
                    }
                }
                else
                {
                    //Log failed cache grab
                }
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
            }

            private bool IsCachedEntryValid(ICacheOutputItem entry)
            {
                return _cacheProvider.IsCachedEntryValid(entry);
            }
        }
    }
}
